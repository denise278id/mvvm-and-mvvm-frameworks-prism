﻿using System.Diagnostics;
using Prism.Mvvm;

namespace IntroToPrism.ViewModels
{
    public class IntroToPrismPageViewModel:BindableBase
    {
        public IntroToPrismPageViewModel()
        {
            Debug.WriteLine($"****{this.GetType().Name}: ctor");
        }
        
    }

}