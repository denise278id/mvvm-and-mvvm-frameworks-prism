﻿using System.Diagnostics;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;
using IntroToPrism.Views;
using IntroToPrism.ViewModels;
using Xamarin.Forms.Xaml;

[assembly:XamlCompilation(XamlCompilationOptions.Compile)]

namespace IntroToPrism
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer=null):base(initializer){ }


 

        protected override void OnInitialized()
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();

            NavigationService.NavigateAsync(nameof(IntroToPrismPage));
        }

        protected override void RegisterTypes(Prism.Ioc.IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(RegisterTypes)}");
            containerRegistry.RegisterForNavigation<IntroToPrismPage, IntroToPrismPageViewModel>();

        }


        protected override void OnStart()
        {
            // Handle when your app starts
            //Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnStart)}");
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
           // Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnSleep)}");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
           // Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnResume)}");
        }
    }
}
