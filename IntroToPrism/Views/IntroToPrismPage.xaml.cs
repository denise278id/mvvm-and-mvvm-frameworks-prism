﻿using System.Diagnostics;
using Xamarin.Forms;

namespace IntroToPrism.Views
{
    public partial class IntroToPrismPage : ContentPage

    {
        public IntroToPrismPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(IntroToPrism)}:  ctor");

            InitializeComponent();
        }

    }
}
